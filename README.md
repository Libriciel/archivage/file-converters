# File Converters
[![build status](https://gitlab.libriciel.fr/file-converters/file-converters/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/file-converters/file-converters/pipelines/latest)
[![coverage report](https://gitlab.libriciel.fr/file-converters/file-converters/badges/master/coverage.svg)](https://asalae2.dev.libriciel.fr/coverage/file-converters/index.html)

## Prérequis

Pour utiliser convenablement cet outil, une installation Ubuntu 16.04 LTS est conseillée.
Une version PHP >= 5.6 est requise (compatible php 7).
Vous devrez en outre vous assurer que tout les packets suivant sont installés pour un fonctionnement optimal sur tout les types de fichiers supportés.
```bash
sudo apt-get install imagemagick handbrake-cli soundconverter libavcodec-extra libav-tools
```

## Installation

```bash
composer config repositories.libriciel/file-converters git git@gitlab.libriciel.fr:file-converters/file-converters.git
composer require libriciel/file-converters ~1.0
```

## Utilisation

L'utilisation est très simple, pour cela, il suffit d'appeler la méthode statique `\File\Converters\FileConverters::convert($filename, $formatDeDestination)` pour obtenir une chaine binaire.
Le 3e paramètre permet d'enregistrer le résultat dans un fichier si besoin (indiquez le chemin).
Le 4e paramètre peut vous permettre d'envoyer des paramètres au callback de conversion.

Exemple d'utilisation :
```php

\File\Converters\FileConverters::convert(
    'uploaded/file.avi',
    'mp4',
    'converted/file.mp4',
    [
        'video' => 'x265',
        'audio' => 'ac3',
    ]
);

$imageBin = \File\Converters\FileConverters::convert(
    'uploaded/image.jpg',
    'png'
);


```

## Conversions possible (testés)

Audio

* aac
* ac3
* aiff
* amr
* au
* flac
* m4a
* mid
* mka
* mp3
* ogg
* ra
* voc
* wav
* wma

Vers

* flac
* ogg
* mp3 (non testé)
* wav

---

Images

* bmp
* gif
* gif animé (1ère image)
* ico
* jpg
* png
* svg
* tiff
* webp

Vers

* jpg
* png

---

Fichier type office/texte

* css
* csv
* doc
* docx
* html
* json
* ods
* odt
* ppt
* pptx
* rtf
* txt
* uot
* xls
* xlsx

Vers

* jpg
* pdf
* png
* html

---

Video, disponible en entrée/sorti avec un grand nombre de codecs

* 3gp
* avi
* flv
* mkv
* mov
* mp4
* mpg
* webm

Vers (codec par defaut : h264 et aac)

* mkv
* mp4