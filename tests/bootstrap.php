<?php
/**
 * Test runner bootstrap.
 *
 * Add additional configuration/setup your application needs when running
 * unit tests in this file.
 */
$basedir = dirname(__DIR__);
if (!is_dir($basedir.DIRECTORY_SEPARATOR.'vendor')) {
    $basedir = dirname(dirname(dirname($basedir)));
}

require $basedir . '/vendor/autoload.php';

if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}
if (!defined('TMP')) {
    define('TMP', sys_get_temp_dir() . DS);
}
if (!defined('SAMPLES_CONVERTERS')) {
    define('SAMPLES_CONVERTERS', $basedir.DS.'vendor'.DS.'libriciel'.DS.'file-converters-tests'.DS.'tests');
}
