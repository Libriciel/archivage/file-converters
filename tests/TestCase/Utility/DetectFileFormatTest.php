<?php

namespace FileConverters\Test\TestCase\Utility;

use FileConverters\Utility\DetectFileFormat;
use PHPUnit\Framework\TestCase;
use ZipStream\Exception\FileNotReadableException;

class DetectFileFormatTest extends TestCase
{
    const SAMPLES = SAMPLES_CONVERTERS.DIRECTORY_SEPARATOR.'samples'.DIRECTORY_SEPARATOR;

    public function testConstruct()
    {
        $this->expectException(FileNotReadableException::class);
        new DetectFileFormat('not_exists');
    }

    public function testGetCategory()
    {
        $detect = new DetectFileFormat(self::SAMPLES.'sample.txt');
        $this->assertEquals('document_text', $detect->getCategory());

        $detect = new DetectFileFormat(self::SAMPLES.'sample.odt');
        $this->assertEquals('document_text', $detect->getCategory());

        $detect = new DetectFileFormat(self::SAMPLES.'sample.pdf');
        $this->assertEquals('document_pdf', $detect->getCategory());

        $detect = new DetectFileFormat(self::SAMPLES.'sample.xls');
        $this->assertEquals('document_calc', $detect->getCategory());

        $detect = new DetectFileFormat(self::SAMPLES.'sample.pptx');
        $this->assertEquals('document_presentation', $detect->getCategory());

        $detect = new DetectFileFormat(self::SAMPLES.'sample.png');
        $this->assertEquals('image', $detect->getCategory());

        $detect = new DetectFileFormat(self::SAMPLES.'sample.mp3');
        $this->assertEquals('audio', $detect->getCategory());

        $detect = new DetectFileFormat(self::SAMPLES.'sample.avi');
        $this->assertEquals('video', $detect->getCategory());

        $detect = new DetectFileFormat(__FILE__);
        $this->assertEmpty($detect->getCategory());
    }

    public function testGetCodecs()
    {
        $detect = new DetectFileFormat(self::SAMPLES.'sample.avi');
        $codecs = $detect->getCodecs();
        $this->assertArrayHasKey('video', $codecs);
        $this->assertArrayHasKey('audio', $codecs);
        $this->assertArrayHasKey('container', $codecs);
        $this->assertEquals('mpeg4', $codecs['video']);
        $this->assertEquals('mp3', $codecs['audio']);
        $this->assertEquals('avi', $codecs['container']);

        $detect = new DetectFileFormat(self::SAMPLES.'sample.mp3');
        $codecs = $detect->getCodecs();
        $this->assertArrayNotHasKey('video', $codecs);
        $this->assertArrayHasKey('audio', $codecs);
        $this->assertArrayHasKey('container', $codecs);
        $this->assertEquals('mp3', $codecs['audio']);
        $this->assertEquals('mp3', $codecs['container']);

        $detect = new DetectFileFormat(__FILE__);
        $this->assertEmpty($detect->getCodecs());
    }

    public function testIs()
    {
        $detect = new DetectFileFormat(self::SAMPLES.'sample.mkv');
        $this->assertFalse($detect->is('unknown format'));
        $this->assertTrue($detect->is('matroska', DetectFileFormat::COMPARE_CONTAINER));
        $this->assertTrue($detect->is('h264', DetectFileFormat::COMPARE_VIDEO_CODEC));
        $this->assertTrue($detect->is('aac', DetectFileFormat::COMPARE_AUDIO_CODEC));
        $this->assertFalse($detect->is('mp3', DetectFileFormat::COMPARE_AUDIO_CODEC));
    }

    public function testRgiCompatible()
    {
        // extensions des fichiers samples
        $compatibles = [
            'txt', 'odt', 'pdfa', 'ods', 'odp', 'png', 'jpg', 'tiff', 'gif', 'svg',
            'jp2', 'aac', 'flac', 'mp3', 'ogg', 'opus', 'mkv', 'mp4', 'webm',
        ];
        $incompatible = [
            'docx', 'pdf', 'xls', 'xlsx', 'pptx', 'bmp', 'ac3', 'avi', 'm2ts',
        ];
        foreach ($compatibles as $ext) {
            $detect = new DetectFileFormat(self::SAMPLES."sample.$ext");
            $this->assertTrue($detect->rgiCompatible(), "RGI incompatible: $ext");
        }
        foreach ($incompatible as $ext) {
            $detect = new DetectFileFormat(self::SAMPLES."sample.$ext");
            $this->assertFalse($detect->rgiCompatible(), "RGI compatible: $ext");
        }
    }
}
