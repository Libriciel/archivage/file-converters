<?php

namespace FileConverters\Test\TestCase\Utility;

use FileConverters\Utility\DetectFileFormat;
use PHPUnit\Framework\TestCase;
use FileValidator\Utility\FileValidator;
use FileConverters\Utility\FileConverters;

class FileConvertersTest extends TestCase
{
    public function testEnv()
    {
        $this->assertTrue(
            class_exists('\FileValidator\Utility\FileValidator'),
            "La bibliotèque FileValidator est requise pour vérifier les fichiers convertis"
        );
    }

    public function testConvertJpgToPng()
    {
        $file = tempnam(sys_get_temp_dir(), 'testunit-');
        $text = "Hello world";
        $jpgFilename = $file . '.jpg';
        $pngFilename = $file . '.png';

        file_put_contents($file, $text);

        $this->assertNotFalse(FileConverters::convert($file, 'jpg', $jpgFilename));
        $this->assertNotFalse(FileConverters::convert($jpgFilename, 'png', $pngFilename));

        $this->assertEquals('image/jpeg', mime_content_type($jpgFilename), "txt converti en jpg");
        $this->assertTrue(FileValidator::check($jpgFilename), "Est une image valide (jpg)");
        $this->assertEquals('image/png', mime_content_type($pngFilename), "jpg converti en png");
        $this->assertTrue(FileValidator::check($pngFilename), "Est une image valide (png)");

        unlink($file);
        unlink($jpgFilename);
        unlink($pngFilename);
    }

    public function testConvertTxtToPdf()
    {
        $file = tempnam(sys_get_temp_dir(), 'testunit-');
        $text = "Hello world";
        $pdfFilename = $file . '.pdf';

        file_put_contents($file, $text);

        $this->assertNotFalse(FileConverters::convert($file, 'pdf', $pdfFilename));

        $this->assertEquals('application/pdf', mime_content_type($pdfFilename), "txt converti en pdf");
        $this->assertTrue(FileValidator::check($pdfFilename), "Est un pdf valide (txt)");

        unlink($file);
        unlink($pdfFilename);
    }

    public function testConvertPttxToPdf()
    {
        $file = SAMPLES_CONVERTERS . DS . 'samples' . DS . 'sample.pptx';
        $this->assertFileExists($file);
        $newFilename = sys_get_temp_dir() . DS . uniqid('testunit-') . '.pdf';
        $this->assertNotFalse(FileConverters::convert($file, 'pdf', $newFilename));

        $this->assertEquals('application/pdf', mime_content_type($newFilename), "pptx converti en pdf");
        $this->assertTrue(FileValidator::check($newFilename), "Est un pdf valide (pttx)");

        unlink($newFilename);
    }

    public function testConvertXlsToPdf()
    {
        $file = SAMPLES_CONVERTERS . DS . 'samples' . DS . 'sample.xls';
        $this->assertFileExists($file);
        $newFilename = sys_get_temp_dir() . DS . uniqid('testunit-') . '.pdf';
        $this->assertNotFalse(FileConverters::convert($file, 'pdf', $newFilename));

        $this->assertEquals('application/pdf', mime_content_type($newFilename), "xls converti en pdf");
        $this->assertTrue(FileValidator::check($newFilename), "Est un pdf valide (xls)");

        unlink($newFilename);
    }

    public function testConvertAviToMkv()
    {
        $file = SAMPLES_CONVERTERS . DS . 'samples' . DS . 'sample.avi';
        $this->assertFileExists($file);
        $newFilename = sys_get_temp_dir() . DS . uniqid('testunit-') . '.mkv';
        $this->assertNotFalse(FileConverters::convert($file, 'mkv', $newFilename));

        $this->assertEquals('video/x-matroska', mime_content_type($newFilename), "avi converti en mkv");
        $this->assertTrue(FileValidator::check($newFilename), "Est une video valide (mkv)");
        unlink($newFilename);

        // copy flux video
        $newFilename = sys_get_temp_dir() . DS . uniqid('testunit-') . '.mkv';
        $this->assertNotFalse(
            FileConverters::convert(
                $file,
                'mkv',
                $newFilename,
                [
                    'video' => 'mpeg4'
                ]
            )
        );

        $this->assertEquals('video/x-matroska', mime_content_type($newFilename), "avi converti en mkv");
        $this->assertTrue(FileValidator::check($newFilename), "Est une video valide (mkv)");
        $detector = new DetectFileFormat($newFilename);
        $codecs = $detector->getCodecs();
        $this->assertNotEmpty($codecs);
        $this->assertArrayHasKey('video', $codecs);
        $this->assertArrayHasKey('audio', $codecs);
        $this->assertArrayHasKey('container', $codecs);
        $this->assertEquals('mpeg4', $codecs['video']);
        $this->assertEquals('aac', $codecs['audio']);
        $this->assertEquals('matroska,webm', $codecs['container']);
        unlink($newFilename);

        // copy flux audio
        $newFilename = sys_get_temp_dir() . DS . uniqid('testunit-') . '.mkv';
        $this->assertNotFalse(
            FileConverters::convert(
                $file,
                'mkv',
                $newFilename,
                [
                    'audio' => 'mp3'
                ]
            )
        );

        $this->assertEquals('video/x-matroska', mime_content_type($newFilename), "avi converti en mkv");
        $this->assertTrue(FileValidator::check($newFilename), "Est une video valide (mkv)");
        $detector = new DetectFileFormat($newFilename);
        $codecs = $detector->getCodecs();
        $this->assertNotEmpty($codecs);
        $this->assertArrayHasKey('video', $codecs);
        $this->assertArrayHasKey('audio', $codecs);
        $this->assertArrayHasKey('container', $codecs);
        $this->assertEquals('h264', $codecs['video']);
        $this->assertEquals('mp3', $codecs['audio']);
        $this->assertEquals('matroska,webm', $codecs['container']);

        unlink($newFilename);
    }

    public function testConvertMp3ToOgg()
    {
        $file = SAMPLES_CONVERTERS . DS . 'samples' . DS . 'sample.mp3';
        $this->assertFileExists($file);
        $newFilename = sys_get_temp_dir() . DS . uniqid('testunit-') . '.ogg';
        $this->assertNotFalse(FileConverters::convert($file, 'ogg', $newFilename));

        $this->assertEquals('audio/ogg', mime_content_type($newFilename), "mp3 converti en ogg");
        $this->assertTrue(FileValidator::check($newFilename), "Est un fichier audio valide (ogg)");

        unlink($newFilename);
    }

    public function testConvertPdfToPdf()
    {
        $file = SAMPLES_CONVERTERS . DS . 'samples' . DS . 'sample.pdf';
        $this->assertFileExists($file);
        $newFilename = sys_get_temp_dir() . DS . uniqid('testunit-') . '.pdf';
        $this->assertNotFalse(FileConverters::convert($file, 'pdf', $newFilename));

        $this->assertEquals('application/pdf', mime_content_type($newFilename), "pdf converti en pdf");
        $this->assertTrue(FileValidator::check($newFilename), "Est un pdf valide (pdf)");
        $this->assertStringContainsString('PDF\/A', json_encode(FileValidator::getPuid($newFilename)));

        unlink($newFilename);
    }

    public function testConvertM2tsToMp4()
    {
        $file = SAMPLES_CONVERTERS . DS . 'samples' . DS . 'sample.m2ts';
        $this->assertFileExists($file);
        $newFilename = sys_get_temp_dir() . DS . uniqid('testunit-') . '.m2ts';
        $this->assertNotFalse(FileConverters::convert($file, 'mp4', $newFilename));

        $this->assertEquals('video/mp4', mime_content_type($newFilename), "m2ts converti en mp4");
        $this->assertTrue(FileValidator::check($newFilename), "Est une video valide (mp4)");
        $this->assertStringContainsString('MPEG-4 Media File', json_encode(FileValidator::getPuid($newFilename)));

        unlink($newFilename);
    }
}
