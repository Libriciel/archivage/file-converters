<?php
/**
 * FileConverters\Utility\DetectFileFormat
 */

namespace FileConverters\Utility;

use Exception;
use FileValidator\Utility\FileValidator;
use ZipStream\Exception\FileNotReadableException;

/**
 * Détection du format d'un fichier
 *
 * @category    File
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class DetectFileFormat
{
    const CATEGORY_TEXT = 'document_text';
    const CATEGORY_PDF = 'document_pdf';
    const CATEGORY_CALC = 'document_calc';
    const CATEGORY_PRESENTATION = 'document_presentation';
    const CATEGORY_IMAGE = 'image';
    const CATEGORY_AUDIO = 'audio';
    const CATEGORY_VIDEO = 'video';

    const COMPARE_FORMAT = 0;
    const COMPARE_CONTAINER = 1;
    const COMPARE_AUDIO_CODEC = 2;
    const COMPARE_VIDEO_CODEC = 3;

    const RGI_TEXT = ['odt', 'text'];
    const RGI_PDF = ['pdfa'];
    const RGI_CALC = ['ods'];
    const RGI_PRESENTATION = ['odp'];
    const RGI_IMAGE = ['png', 'jpg', 'tiff', 'gif', 'svg'];
    const RGI_AUDIO_CONTAINER = ['flac', 'ogg', 'mp3', 'aac'];
    const RGI_AUDIO_CODEC = ['flac', 'vorbis', 'opus', 'mp3', 'aac'];
    const RGI_VIDEO_CONTAINER = ['mp4', 'matroska', 'mpegts'];
    const RGI_VIDEO_CODEC = ['vp8', 'h264'];
    const RGI_VIDEO_AUDIO_CODEC = ['vorbis', 'aac', 'flac', 'mp3'];

    /**
     * @var string chemin vers le fichier
     */
    public $filename;
    /**
     * @var string type mime du fichier
     */
    public $mime;
    /**
     * @var array codecs pour un fichier audio/video
     */
    public $codecs;
    /**
     * @var string categorie du fichier (text, audio, image...)
     */
    public $category;
    /**
     * @var array retour de siegfried
     */
    public $puid;

    /**
     * DetectFileFormat constructor.
     * @param string $filename
     * @throws FileNotReadableException
     */
    public function __construct(string $filename)
    {
        if (!is_readable($filename)) {
            throw new FileNotReadableException($filename);
        }
        $this->filename = $filename;
        $this->mime = mime_content_type($filename);

        // cas particuliers
        if (in_array($this->mime, ['application/octet-stream', 'application/pdf', 'application/vnd.rn-realmedia'])) {
            switch ($this->getPuid('format')) {
                case 'M2TS': // application/octet-stream
                    $this->mime = 'video/mpeg2ts'; // mime arbitraire
                    break;
                case 'Adobe Illustrator': // application/pdf
                    $this->mime = 'image/adobe-illustrator';
                    break;
                case 'RealMedia': // application/vnd.rn-realmedia
                    $this->mime = 'audio/realmedia';
                    break;
            }
        }
    }

    /**
     * Donne la catégorie du fichier, renvoi null s'il ne trouve pas
     * @return string|null
     */
    public function getCategory(): string
    {
        if (is_string($this->category)) {
            return $this->category;
        }
        $found = null;
        foreach (FileConverters::$mimes as $type => $regex) {
            if (preg_match($regex, $this->mime)) {
                $found = $type;
                break;
            }
        }
        switch ($found) {
            case 'texts':
            case 'libre_office_writer':
            case 'ms_office_writer':
                $this->category = self::CATEGORY_TEXT;
                break;
            case 'pdfs':
                $this->category = self::CATEGORY_PDF;
                break;
            case 'libre_office_calc':
            case 'ms_office_calc':
                $this->category = self::CATEGORY_CALC;
                break;
            case 'libre_office_pres':
            case 'ms_office_pres':
                $this->category = self::CATEGORY_PRESENTATION;
                break;
            case 'images':
                $this->category = self::CATEGORY_IMAGE;
                break;
            case 'videos':
                $codecs = $this->getCodecs();
                if (isset($codecs['video']) && $codecs['video'] === 'ansi') {
                    $this->category = '';
                    break;
                }
                if (isset($codecs['video'])) {
                    $this->category = self::CATEGORY_VIDEO;
                } else { // cas .mka -> video/x-matroska sur fichier audio
                    $this->category = isset($codecs['audio'])
                        ? self::CATEGORY_AUDIO
                        : '';
                }
                break;
            case 'audios':
                $this->category = self::CATEGORY_AUDIO;
                break;
            default:
                $this->category = '';
        }
        return $this->category;
    }

    /**
     * Donne les codecs d'un fichier video/audio
     * @return array ['video' => string, 'audio' => string, 'container' => string]
     */
    public function getCodecs(): array
    {
        if (is_array($this->codecs)) {
            return $this->codecs;
        }
        exec(
            sprintf(
                'ffprobe -v error -show_format -show_streams %s 2>/dev/null',
                escapeshellarg($this->filename)
            ),
            $out,
            $code
        );
        if ($code !== 0) {
            $this->codecs = [];
            return $this->codecs;
        }

        // formate la réponse pour être compatible ini
        for ($i = 0; $i < count($out); $i++) {
            if (substr($out[$i], 0, 2) === '[/') {
                $out[$i] = '';
            } elseif (preg_match('/^\[(.*)]/', $out[$i], $m)
                && isset($out[$i +1])
                && mb_strpos($out[$i +1], 'index=') === 0
            ) {
                $index = mb_substr($out[$i +1], 6); // 6 = strlen('index=')
                $out[$i] = sprintf('[%s_%d]', $m[1], $index);
            }
        }

        $str = implode("\n", $out);
        $str = preg_replace('/^\[\/.*/', '', $str);
        $probe = parse_ini_string($str, true, INI_SCANNER_RAW);
        $this->codecs = [];
        foreach ($probe as $stream) {
            if (isset($stream['codec_type']) && isset($stream['codec_name'])) {
                $this->codecs[$stream['codec_type']] = $stream['codec_name'];
            } elseif (isset($stream['format_name'])) {
                $this->codecs['container'] = $stream['format_name'];
            }
        }
        return $this->codecs;
    }

    /**
     * Permet de savoir si le fichier match un type spécifique
     * @param string $format
     * @param int    $compare
     * @return bool
     */
    public function is(string $format, int $compare = self::COMPARE_FORMAT): bool
    {
        $fn = 'is'.ucfirst($format);
        if (!method_exists($this, $fn)) {
            return false;
        }
        return call_user_func([$this, $fn], $compare);
    }

    /**
     * Vrai si le fichier est d'un format compatible avec le RGI
     * @return bool
     */
    public function rgiCompatible(): bool
    {
        $category = $this->getCategory();
        $compatible = false;
        switch ($category) {
            case self::CATEGORY_TEXT:
                $compatible = $this->compareRgiGeneric(self::RGI_TEXT);
                break;
            case self::CATEGORY_PDF:
                $compatible = $this->compareRgiGeneric(self::RGI_PDF);
                break;
            case self::CATEGORY_CALC:
                $compatible = $this->compareRgiGeneric(self::RGI_CALC);
                break;
            case self::CATEGORY_PRESENTATION:
                $compatible = $this->compareRgiGeneric(self::RGI_PRESENTATION);
                break;
            case self::CATEGORY_IMAGE:
                $compatible = $this->compareRgiGeneric(self::RGI_IMAGE);
                break;
            case self::CATEGORY_AUDIO:
                $containerCompatible = $this->compareRgiGeneric(
                    self::RGI_AUDIO_CONTAINER,
                    self::COMPARE_CONTAINER
                );
                $codecAudioCompatible = $this->compareRgiGeneric(
                    self::RGI_AUDIO_CODEC,
                    self::COMPARE_AUDIO_CODEC
                );
                $compatible = $containerCompatible && $codecAudioCompatible;
                break;
            case self::CATEGORY_VIDEO:
                $containerCompatible = $this->compareRgiGeneric(
                    self::RGI_VIDEO_CONTAINER,
                    self::COMPARE_CONTAINER
                );
                $codecCompatible = $this->compareRgiGeneric(
                    self::RGI_VIDEO_CODEC,
                    self::COMPARE_VIDEO_CODEC
                );
                $codecAudioCompatible = $this->compareRgiGeneric(
                    self::RGI_VIDEO_AUDIO_CODEC,
                    self::COMPARE_AUDIO_CODEC
                );
                $compatible = $containerCompatible && $codecCompatible && $codecAudioCompatible;
                break;
        }
        return $compatible;
    }

    /**
     * Vrai si le fichier a un format dans la liste
     * @param array $list
     * @param int   $compare
     * @return bool
     */
    private function compareRgiGeneric(array $list, int $compare = self::COMPARE_FORMAT): bool
    {
        foreach ($list as $type) {
            if (self::is($type, $compare)) {
                return true;
            }
        }
        return false;
    }

    /**
     * detect callback
     * @return bool
     */
    public function isText(): bool
    {
        return preg_match('/^(text\/(plain|rtf)|application\/octet-stream)(\+.*)?$/', $this->mime);
    }

    /**
     * detect callback
     * @return bool
     */
    public function isOdt(): bool
    {
        return preg_match('/^application\/vnd\.oasis\.opendocument\.(text|writer)(\+.*)?$/', $this->mime);
    }

    /**
     * detect callback
     * @return bool
     * @throws Exception
     */
    public function isPdfa(): bool
    {
        $puid = $this->getPuid('format');
        return mb_strpos($puid, 'PDF/A') !== false;
    }

    /**
     * detect callback
     * @return bool
     */
    public function isOds(): bool
    {
        return preg_match('/^application\/vnd\.oasis\.opendocument\.(spreadsheet|calc)(\+.*)?$/', $this->mime);
    }

    /**
     * detect callback
     * @return bool
     */
    public function isOdp(): bool
    {
        return preg_match('/^application\/vnd\.oasis\.opendocument\.(presentation|impress)(\+.*)?$/', $this->mime);
    }

    /**
     * detect callback
     * @return bool
     */
    public function isPng(): bool
    {
        return preg_match('/^image\/.*png(\+.*)?$/', $this->mime);
    }

    /**
     * detect callback
     * @return bool
     */
    public function isJpg(): bool
    {
        return preg_match('/^image\/.*(jpe?g|jp2)(\+.*)?$/', $this->mime);
    }

    /**
     * detect callback
     * @return bool
     */
    public function isTiff(): bool
    {
        return preg_match('/^image\/.*tiff(\+.*)?$/', $this->mime);
    }

    /**
     * detect callback
     * @return bool
     */
    public function isGif(): bool
    {
        return preg_match('/^image\/.*gif(\+.*)?$/', $this->mime);
    }

    /**
     * detect callback
     * @return bool
     */
    public function isSvg(): bool
    {
        return preg_match('/^image\/.*svg(\+.*)?$/', $this->mime);
    }

    /**
     * detect callback
     * @param int $compare
     * @return bool
     */
    public function isFlac(int $compare): bool
    {
        $codecs = $this->getCodecs();
        $str = '';
        if ($codecs && isset($codecs['container']) && $compare === self::COMPARE_CONTAINER) {
            $str = $codecs['container'];
        } elseif ($codecs && isset($codecs['audio']) && $compare === self::COMPARE_AUDIO_CODEC) {
            $str = $codecs['audio'];
        }
        return strpos($str, 'flac') !== false;
    }

    /**
     * detect callback
     * @param int $compare
     * @return bool
     */
    public function isOgg(int $compare): bool
    {
        $codecs = $this->getCodecs();
        $str = '';
        if ($codecs && isset($codecs['container']) && $compare === self::COMPARE_CONTAINER) {
            $str = $codecs['container'];
        }
        return strpos($str, 'ogg') !== false;
    }

    /**
     * detect callback
     * @param int $compare
     * @return bool
     */
    public function isMp3(int $compare): bool
    {
        $codecs = $this->getCodecs();
        $str = '';
        if ($codecs && isset($codecs['container']) && $compare === self::COMPARE_CONTAINER) {
            $str = $codecs['container'];
        } elseif ($codecs && isset($codecs['audio']) && $compare === self::COMPARE_AUDIO_CODEC) {
            $str = $codecs['audio'];
        }
        return strpos($str, 'mp3') !== false;
    }

    /**
     * detect callback
     * @param int $compare
     * @return bool
     */
    public function isAac(int $compare): bool
    {
        $codecs = $this->getCodecs();
        $str = '';
        if ($codecs && isset($codecs['container']) && $compare === self::COMPARE_CONTAINER) {
            $str = $codecs['container'];
        } elseif ($codecs && isset($codecs['audio']) && $compare === self::COMPARE_AUDIO_CODEC) {
            $str = $codecs['audio'];
        }
        return strpos($str, 'aac') !== false;
    }

    /**
     * detect callback
     * @param int $compare
     * @return bool
     */
    public function isVorbis(int $compare): bool
    {
        $codecs = $this->getCodecs();
        $str = '';
        if ($codecs && isset($codecs['audio']) && $compare === self::COMPARE_AUDIO_CODEC) {
            $str = $codecs['audio'];
        }
        return strpos($str, 'vorbis') !== false;
    }

    /**
     * detect callback
     * @param int $compare
     * @return bool
     */
    public function isOpus(int $compare): bool
    {
        $codecs = $this->getCodecs();
        $str = '';
        if ($codecs && isset($codecs['audio']) && $compare === self::COMPARE_AUDIO_CODEC) {
            $str = $codecs['audio'];
        }
        return strpos($str, 'opus') !== false;
    }

    /**
     * detect callback
     * @param int $compare
     * @return bool
     */
    public function isMp4(int $compare): bool
    {
        $codecs = $this->getCodecs();
        $str = '';
        if ($codecs && isset($codecs['container']) && $compare === self::COMPARE_CONTAINER) {
            $str = $codecs['container'];
        }
        return strpos($str, 'mp4') !== false;
    }

    /**
     * detect callback
     * @param int $compare
     * @return bool
     */
    public function isMatroska(int $compare): bool
    {
        $codecs = $this->getCodecs();
        $str = '';
        if ($codecs && isset($codecs['container']) && $compare === self::COMPARE_CONTAINER) {
            $str = $codecs['container'];
        }
        return strpos($str, 'matroska') !== false;
    }

    /**
     * detect callback
     * @param int $compare
     * @return bool
     */
    public function isMpegts(int $compare): bool
    {
        $codecs = $this->getCodecs();
        $str = '';
        if ($codecs && isset($codecs['container']) && $compare === self::COMPARE_CONTAINER) {
            $str = $codecs['container'];
        }
        return strpos($str, 'mpegts') !== false;
    }

    /**
     * detect callback
     * @param int $compare
     * @return bool
     */
    public function isVp8(int $compare): bool
    {
        $codecs = $this->getCodecs();
        $str = '';
        if ($codecs && isset($codecs['video']) && $compare === self::COMPARE_VIDEO_CODEC) {
            $str = $codecs['video'];
        }
        return strpos($str, 'vp8') !== false;
    }

    /**
     * detect callback
     * @param int $compare
     * @return bool
     */
    public function isH264(int $compare): bool
    {
        $codecs = $this->getCodecs();
        $str = '';
        if ($codecs && isset($codecs['video']) && $compare === self::COMPARE_VIDEO_CODEC) {
            $str = $codecs['video'];
        }
        return strpos($str, 'h264') !== false;
    }

    /**
     * Donne le pronom du fichier
     * @return array|string
     * @throws Exception
     */
    public function getPuid(string $cat = null)
    {
        if ($this->puid === null) {
            $puid = FileValidator::getPuid($this->filename);
            $this->puid = $puid ? $puid[0] : [];
        }
        if ($cat) {
            return $this->puid[$cat] ?? '';
        }
        return $this->puid;
    }
}
