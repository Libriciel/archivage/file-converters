<?php
/**
 * FileConverters\Utility\FileConverters
 */

namespace FileConverters\Utility;

use Exception;
use ZipStream\Exception\FileNotReadableException;

/**
 * Conversion d'un fichier
 *
 * Permet de modifier un fichier vers un autre format au choix
 *
 * @category    File
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class FileConverters
{
    /**
     * Liste les callbacks pour convertir vers une extension en particulier
     *
     * match : type de fichiers @see self::$mimes
     * callback : callable
     *
     * @var array
     */
    public static $callbacks = [
        'jpg' => [
            'match' => [
                'images',
                'libre_office_writer',
                'ms_office_writer',
                'libre_office_calc',
                'ms_office_calc',
                'libre_office_pres',
                'ms_office_pres',
                'texts',
                'htmls'
            ],
            'callback' => '\FileConverters\Utility\FileConverters::convertToJpg'
        ],
        'png' => [
            'match' => [
                'images',
                'libre_office_writer',
                'ms_office_writer',
                'libre_office_calc',
                'ms_office_calc',
                'libre_office_pres',
                'ms_office_pres',
                'texts',
                'htmls'
            ],
            'callback' => '\FileConverters\Utility\FileConverters::convertToPng'
        ],
        'mp4' => [
            'match' => ['videos'],
            'callback' => '\FileConverters\Utility\FileConverters::convertToMp4'
        ],
        'mkv' => [
            'match' => ['videos'],
            'callback' => '\FileConverters\Utility\FileConverters::convertToMkv'
        ],
        'ods' => [
            'match' => ['libre_office_calc', 'ms_office_calc'],
            'callback' => '\FileConverters\Utility\FileConverters::convertToOds'
        ],
        'odp' => [
            'match' => ['libre_office_pres', 'ms_office_pres'],
            'callback' => '\FileConverters\Utility\FileConverters::convertToOdp'
        ],
        'odt' => [
            'match' => ['libre_office_writer', 'ms_office_writer', 'texts', 'htmls'],
            'callback' => '\FileConverters\Utility\FileConverters::convertToOdt'
        ],
        'html' => [
            'match' => [
                'libre_office_writer',
                'ms_office_writer',
                'libre_office_calc',
                'ms_office_calc',
                'libre_office_pres',
                'ms_office_pres',
                'texts',
                'htmls'
            ],
            'callback' => '\FileConverters\Utility\FileConverters::convertToHtml'
        ],
        'flac' => [
            'match' => ['audios'],
            'callback' => '\FileConverters\Utility\FileConverters::convertToFlac'
        ],
        'ogg' => [
            'match' => ['audios'],
            'callback' => '\FileConverters\Utility\FileConverters::convertToOgg'
        ],
        'wav' => [
            'match' => ['audios'],
            'callback' => '\FileConverters\Utility\FileConverters::convertToWav'
        ],
        'pdf' => [
            'match' => [
                'pdfs',
                'libre_office_writer',
                'ms_office_writer',
                'libre_office_calc',
                'ms_office_calc',
                'libre_office_pres',
                'ms_office_pres',
                'texts',
                'htmls'
            ],
            'callback' => '\FileConverters\Utility\FileConverters::convertToPdf'
        ]
    ];

    /**
     * Associe des codes mime avec des types de fichiers convertissable
     *
     * @var array
     */
    public static $mimes = [
        'images' => '/^image\/.*$/',
        'videos' => '/^(video\/.*)|application\/octet-stream(\+.*)?$/',
        'audios' => '/^audio\/.*$/',
        'libre_office_writer' => '/^application\/(vnd\.(oasis\.opendocument|sun\.xml|stardivision)\.|x-star)(text|writer)(\+.*)?$/',
        'ms_office_writer' => '/^application\/(msword|vnd\.openxmlformats\-officedocument\.wordprocessingml\.document|vnd\.ms-word\.document.*)(\+.*)?$/',
        'libre_office_calc' => '/^application\/(vnd\.(oasis\.opendocument|sun\.xml|stardivision)\.|x-star)(spreadsheet|calc)(\+.*)?$/',
        'ms_office_calc' => '/^application\/vnd\.(ms\-excel(\.sheet.*)?|openxmlformats\-officedocument\.spreadsheetml\.sheet)(\+.*)?$/',
        'libre_office_pres' => '/^application\/(vnd\.(oasis\.opendocument|sun\.xml|stardivision)\.|x-star)(presentation|impress)(\+.*)?$/',
        'ms_office_pres' => '/^application\/vnd\.(ms\-powerpoint(\.presentation.*)?|openxmlformats\-officedocument\.presentationml\.presentation)(\+.*)?$/',
        'texts' => '/^(text\/(plain|rtf)|application\/octet-stream)(\+.*)?$/',
        'htmls' => '/^(text\/.*html|application\/xml)(\+.*)?$/',
        'pdfs' => '/^application\/(x\-)?pdf(\+.*)?$/',
    ];

    /**
     * Liste des formats reconnues mais que FileConverters n'est pas capable de convertir
     */
    public static $blacklist = [
        '/^audio\/midi(\+.*)?$/',
        '/^image\/adobe\-illustrator$/',
    ];

    /**
     * A des fins de debug, permet de lister les commandes exécutés,
     * leurs valeurs de retour, les erreurs et les codes de retour
     *
     * @var array
     */
    public static $logs = [];

    /**
     * Converti un fichier vers un autre format
     *
     * @param string $filename chemin vers le fichier à convertir
     * @param string $convertTo extension du fichier une fois converti
     * @param null   $mv destination où écrire le fichier converti (optionnel)
     * @param mixed  $params paramètres à envoyer au callback
     * @return boolean|string fichier binaire / chemin vers le fichier si $mv
     * @throws FileNotReadableException
     */
    public static function convert($filename, $convertTo, $mv = null, array $params = [])
    {
        if (!isset(static::$callbacks[$convertTo])
            || !is_readable($filename)
            || !self::canConvert($filename)
        ) {
            return false;
        }
        if ($mv) {
            $params['output'] = $mv;
            $params['unlink'] = false;
        }

        $bin = call_user_func_array(static::$callbacks[$convertTo]['callback'], [$filename, &$params]);

        if (empty($bin)) {
            return false;
        }

        return $bin;
    }

    /**
     * Permet à partir d'un nom de fichier de connaitre la liste des conversions
     * possible.
     *
     * @param string $filename
     * @return array|bool
     */
    public static function matchFormats($filename)
    {
        if (!is_readable($filename)) {
            return false;
        }
        $mime = mime_content_type($filename);
        $results = [];
        foreach (static::$mimes as $format => $regex) {
            if (!preg_match($regex, $mime)) {
                continue;
            }
            foreach (static::$callbacks as $key => $values) {
                if (in_array($format, $values['match'])) {
                    $results[] = $key;
                }
            }
        }

        return $results;
    }

    /**
     * Callback
     *
     * @param string $filename
     * @param array  $params
     * @return bool|string fichier binaire
     * @throws FileNotReadableException
     * @internal param array $params
     */
    public static function convertToJpg($filename, array $params = [])
    {
        $mime = mime_content_type($filename);
        $type = (new DetectFileFormat($filename))->getCategory();

        if ($type === 'image' || preg_match(static::$mimes['images'], $mime)) {
            return static::imageToImage($filename, 'jpg', $params);
        } elseif (in_array('jpg', static::matchFormats($filename))) {
            $tmpFile = self::tmpName('jpg');
            static::officeTo($filename, 'png', ['output' => $tmpFile]);
            $bin = static::imageToImage($tmpFile, 'jpg', $params);
            unlink($tmpFile);
            return $bin;
        }

        return false;
    }

    /**
     * Callback
     *
     * @param string $filename
     * @param array  $params
     * @return bool|string fichier binaire
     * @throws FileNotReadableException
     * @internal param array $params
     */
    public static function convertToPng($filename, array $params = [])
    {
        $mime = mime_content_type($filename);
        $type = (new DetectFileFormat($filename))->getCategory();

        if ($type === 'image' || preg_match(static::$mimes['images'], $mime)) {
            return static::imageToImage($filename, 'png', $params);
        } elseif (in_array('png', static::matchFormats($filename))) {
            return static::officeTo($filename, 'png', $params);
        }

        return false;
    }

    /**
     * Callback
     *
     * @param string $filename
     * @param array  $params
     * @return boolean|string fichier binaire
     * @throws FileNotReadableException
     */
    public static function convertToMp4($filename, array $params = [])
    {
        $mime = mime_content_type($filename);
        $type = (new DetectFileFormat($filename))->getCategory();

        if ($type === 'image' || preg_match(static::$mimes['videos'], $mime)) {
            return static::videoToVideo($filename, 'mp4', $params);
        }

        return false;
    }

    /**
     * Callback
     *
     * @param string $filename
     * @param array  $params
     * @return boolean|string fichier binaire
     * @throws FileNotReadableException
     */
    public static function convertToMkv($filename, array $params = [])
    {
        $mime = mime_content_type($filename);
        $type = (new DetectFileFormat($filename))->getCategory();

        if ($type === 'video' || preg_match(static::$mimes['videos'], $mime)) {
            return static::videoToVideo($filename, 'mkv', $params);
        }

        return false;
    }

    /**
     * Callback
     *
     * @param string $filename
     * @param array  $params
     * @return bool|string fichier binaire
     * @throws FileNotReadableException
     * @internal param array $params
     */
    public static function convertToPdf($filename, array $params = [])
    {
        $mime = mime_content_type($filename);
        $type = (new DetectFileFormat($filename))->getCategory();

        if ($type === 'document_pdf' || preg_match(static::$mimes['pdfs'], $mime)) {
            return static::pdfToPdf($filename, 'pdf', $params);
        } elseif (in_array('pdf', static::matchFormats($filename))) {
            return static::officeToPdf($filename, 'pdf', $params);
        }

        return false;
    }

    /**
     * Callback
     *
     * @param string $filename
     * @param array  $params
     * @return bool|string fichier binaire
     * @throws FileNotReadableException
     * @internal param array $params
     */
    public static function convertToOdt($filename, array $params = [])
    {
        $type = (new DetectFileFormat($filename))->getCategory();
        if ($type === 'document_text' || in_array('odt', static::matchFormats($filename))) {
            return static::officeToOffice($filename, 'odt', $params);
        }

        return false;
    }

    /**
     * Callback
     *
     * @param string $filename
     * @param array  $params
     * @return bool|string fichier binaire
     * @throws FileNotReadableException
     * @internal param array $params
     */
    public static function convertToOds($filename, array $params = [])
    {
        $type = (new DetectFileFormat($filename))->getCategory();
        if ($type === 'document_calc' || in_array('ods', static::matchFormats($filename))) {
            return static::officeToOffice($filename, 'ods', $params);
        }

        return false;
    }

    /**
     * Callback
     *
     * @param string $filename
     * @param array  $params
     * @return bool|string fichier binaire
     * @throws FileNotReadableException
     * @internal param array $params
     */
    public static function convertToOdp($filename, array $params = [])
    {
        $type = (new DetectFileFormat($filename))->getCategory();
        if ($type === 'document_presentation' || in_array('odp', static::matchFormats($filename))) {
            return static::officeToOffice($filename, 'odp', $params);
        }

        return false;
    }

    /**
     * Callback
     *
     * @param string $filename
     * @param array  $params
     * @return bool|string fichier binaire
     * @internal param array $params
     */
    public static function convertToHtml($filename, array $params = [])
    {
        if (in_array('html', static::matchFormats($filename))) {
            return static::officeTo($filename, 'html', $params);
        }

        return false;
    }

    /**
     * Callback
     *
     * @param string $filename
     * @param array  $params
     * @return bool|string fichier binaire
     * @throws FileNotReadableException
     * @internal param array $params
     */
    public static function convertToFlac($filename, array $params = [])
    {
        $mime = mime_content_type($filename);
        $type = (new DetectFileFormat($filename))->getCategory();

        if ($type === 'audio' || preg_match(static::$mimes['audios'], $mime)) {
            return static::audioToAudio($filename, 'flac', $params);
        }

        return false;
    }

    /**
     * Callback
     *
     * @param string $filename
     * @param array  $params
     * @return bool|string fichier binaire
     * @throws FileNotReadableException
     * @internal param array $params
     */
    public static function convertToOgg($filename, array $params = [])
    {
        $mime = mime_content_type($filename);
        $type = (new DetectFileFormat($filename))->getCategory();

        if ($type === 'audio' || preg_match(static::$mimes['audios'], $mime)) {
            return static::audioToAudio($filename, 'ogg', $params);
        }

        return false;
    }

    /**
     * Callback
     *
     * NOTE: le mp3 n'est pas un format libre, vous devez avoir la license pour
     * l'utiliser
     *
     * @param string $filename
     * @param array  $params
     * @return bool|string fichier binaire
     * @throws FileNotReadableException
     * @internal param array $params
     */
    public static function convertToMp3($filename, array $params = [])
    {
        $mime = mime_content_type($filename);
        $type = (new DetectFileFormat($filename))->getCategory();

        if ($type === 'audio' || preg_match(static::$mimes['audios'], $mime)) {
            return static::audioToAudio($filename, 'mp3', $params);
        }

        return false;
    }

    /**
     * Callback
     *
     * @param string $filename
     * @param array  $params
     * @return bool|string fichier binaire
     * @throws FileNotReadableException
     * @internal param array $params
     */
    public static function convertToWav($filename, array $params = [])
    {
        $mime = mime_content_type($filename);
        $type = (new DetectFileFormat($filename))->getCategory();

        if ($type === 'audio' || preg_match(static::$mimes['audios'], $mime)) {
            return static::audioToAudio($filename, 'wav', $params);
        }

        return false;
    }

    /**
     * Permet de savoir si FileConverters est capable de convertir un fichier
     * @param string $filename
     * @return bool
     * @throws FileNotReadableException
     */
    public static function canConvert(string $filename): bool
    {
        $detector = new DetectFileFormat($filename);
        $type = $detector->getCategory();
        if (!$type) {
            return false;
        }
        $mime = $detector->mime;
        foreach (self::$blacklist as $regex) {
            if (preg_match($regex, $mime)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Converti un pdf/office vers du pdf/a
     *
     * @param string $filename
     * @param string $type
     * @param array  $params
     * @return boolean|string
     */
    protected static function officeToPdf($filename, $type = 'pdf', array $params = [])
    {
        $params += [
            'output' => self::tmpName($type),
            'unlink' => empty($params['output']),
        ];
        $cmd = sprintf(
            "unoconv -f %s -eSelectPdfVersion=1 -o %s %s",
            $type,
            escapeshellarg($params['output']),
            escapeshellarg($filename)
        );
        if (!empty($params['return_cmd'])) {
            return $cmd;
        }
        static::$logs[] = static::exec($cmd, $strout, $code);

        return self::handleOutput($code, $params);
    }

    /**
     * Converti une image vers une autre image
     *
     * @param string $filename
     * @param string $type ex: jpg, png, gif...
     * @param array  $params
     * @return boolean|string
     */
    protected static function imageToImage($filename, $type = 'png', array $params = [])
    {
        $params += [
            'output' => self::tmpName($type),
            'unlink' => empty($params['output']),
        ];
        $cmd = sprintf(
            "convert %s[0] %s",
            escapeshellarg($filename),
            escapeshellarg($params['output'])
        );
        if (!empty($params['return_cmd'])) {
            return $cmd;
        }
        static::$logs[] = static::exec($cmd, $strout, $code);

        return self::handleOutput($code, $params);
    }

    /**
     * Converti une video vers un autre type de video
     *
     * Note: ffmpeg provoque des erreurs en cas d'encodage du flux video en h264
     *       du fichier de test.
     *       Par contre handbrake n'est pas capable de copier le flux video
     *
     * @see https://handbrake.fr/docs/en/1.0.0/cli/cli-guide.html
     *
     * @param string $filename
     * @param string $type
     * @param array  $params ['output' => path, 'unlink' => bool, 'video' => 'h264',
     *                       'audio' => 'aac', 'transcoder' => {auto|ffmpeg|handbrake}]
     *                       ['transcoder' => 'auto'] va privilégier handbrake mais
     *                       il sélectionnera ffmpeg si le flux video est déjà au format voulu
     * @return boolean|string
     * @throws FileNotReadableException
     */
    protected static function videoToVideo($filename, $type = 'mp4', array $params = [])
    {
        $params += [
            'output' => self::tmpName($type),
            'unlink' => empty($params['output']),
            'video' => 'h264',
            'audio' => 'aac',
            'transcoder' => 'auto'
        ];

        if ($params['transcoder'] === 'auto') {
            $detector = new DetectFileFormat($filename);
            $formats = $detector->getCodecs();
            if (isset($formats['video']) && $formats['video'] === $params['video']) {
                $params['transcoder'] = 'ffmpeg';
                $params['video'] = 'copy';
            } else {
                $params['transcoder'] = 'handbrake';
            }
            if (isset($formats['audio']) && $formats['audio'] === $params['audio']) {
                $params['audio'] = 'copy';
            }
        }

        if ($params['transcoder'] === 'ffmpeg') {
            switch ($params['video']) {
                case 'h264':
                    $videoEncoder = 'libx264';
                    break;
                case 'vp8':
                    $videoEncoder = 'libvpx';
                    break;
                default:
                    $videoEncoder = $params['video'];
            }
            switch ($params['audio']) {
                case 'vorbis':
                    $audioEncoder = 'libvorbis';
                    break;
                default:
                    $audioEncoder = $params['audio'];
            }
            $cmd = sprintf(
                "ffmpeg -i %s -c:v %s -c:a %s %s",
                escapeshellarg($filename),
                escapeshellarg($videoEncoder),
                escapeshellarg($audioEncoder),
                escapeshellarg($params['output'])
            );
            if (!empty($params['return_cmd'])) {
                return $cmd;
            }
        } elseif ($params['transcoder'] === 'handbrake') {
            switch ($params['video']) {
                case 'h264':
                    $videoEncoder = 'x264';
                    break;
                case 'h265':
                    $videoEncoder = 'x265';
                    break;
                default:
                    $videoEncoder = $params['video'];
            }
            switch ($params['audio']) {
                case 'aac':
                    $audioEncoder = 'copy:aac';
                    break;
                case 'ac3':
                    $audioEncoder = 'copy:ac3';
                    break;
                case 'mp3':
                    $audioEncoder = 'copy:mp3';
                    break;
                case 'flac':
                    $audioEncoder = 'copy:flac';
                    break;
                default:
                    $audioEncoder = $params['audio'];
            }
            $cmd = sprintf(
                "HandBrakeCLI -i %s -o %s --encoder %s --aencoder %s",
                escapeshellarg($filename),
                escapeshellarg($params['output']),
                escapeshellarg($videoEncoder),
                escapeshellarg($audioEncoder)
            );
            if (!empty($params['return_cmd'])) {
                return $cmd;
            }
        } else {
            throw new Exception('invalid transcoder');
        }

        static::$logs[] = static::exec($cmd, $strout, $code);

        if ($code === 127) {
            trigger_error("Vous devez installer ffmpeg pour effectuer la conversion video. Essayez sudo apt-get install ffmpeg");
            return false;
        }

        return self::handleOutput($code, $params);
    }

    /**
     * Converti un fichier office vers un autre type
     *
     * @param string $filename
     * @param string $type
     * @param array  $params
     * @return boolean|string
     */
    protected static function officeTo($filename, $type = 'pdf', array $params = [])
    {
        $params += [
            'output' => sys_get_temp_dir()
                .DIRECTORY_SEPARATOR
                .basename($filename, pathinfo($filename, PATHINFO_EXTENSION))
                .'.'
                .$type,
            'unlink' => empty($params['output']),
        ];
        $outdir = sys_get_temp_dir().DIRECTORY_SEPARATOR.uniqid('convert-');
        mkdir($outdir);

        $cmd = sprintf(
            "libreoffice --headless --convert-to %s --outdir %s %s",
            $type,
            escapeshellarg($outdir),
            escapeshellarg($filename)
        );
        if (!empty($params['return_cmd'])) {
            return $cmd;
        }

        static::$logs[] = static::exec($cmd, $strout, $code);

        $files = glob($outdir.DIRECTORY_SEPARATOR.'*');
        if ($files && count($files) === 1) {
            rename($files[0], $params['output']);
            rmdir($outdir);
        }

        return self::handleOutput($code, $params);
    }

    /**
     * Converti un fichier office vers un autre office
     *
     * @param string $filename
     * @param string $type
     * @param array  $params
     * @return boolean|string
     */
    protected static function officeToOffice($filename, $type = 'odt', array $params = [])
    {
        $params += [
            'output' => self::tmpName($type),
            'unlink' => empty($params['output']),
        ];

        $cmd = sprintf(
            "unoconv -f %s -o %s %s",
            $type,
            escapeshellarg($params['output']),
            escapeshellarg($filename)
        );
        if (!empty($params['return_cmd'])) {
            return $cmd;
        }

        static::$logs[] = static::exec($cmd, $strout, $code);

        return self::handleOutput($code, $params);
    }

    /**
     * Créer un fichier temporaire et renvoi son path
     *
     * @param string $bin
     * @return string filename
     */
    protected static function tmpFile($bin): string
    {
        $tmpfname = tempnam(sys_get_temp_dir(), 'tmpfileconverters');
        $handle = fopen($tmpfname, "w");
        fwrite($handle, $bin);
        fclose($handle);

        return $tmpfname;
    }

    /**
     * Converti un fichier audio vers un autre type
     *
     * @param string $filename
     * @param string $type
     * @param array  $params
     * @return boolean|string
     */
    protected static function audioToAudio($filename, $type = 'ogg', array $params = [])
    {
        $params += [
            'output' => self::tmpName($type),
            'unlink' => empty($params['output']),
        ];
        is_file($params['output']) && unlink($params['output']);

        $cmd = sprintf(
            "ffmpeg -i %s %s",
            escapeshellarg($filename),
            escapeshellarg($params['output'])
        );
        if (!empty($params['return_cmd'])) {
            return $cmd;
        }

        static::$logs[] = static::exec($cmd, $strout, $code);

        if ($code === 127) {
            trigger_error("Vous devez installer ffmpeg pour effectuer la conversion audio. Essayez sudo apt install ffmpeg");
            return false;
        }

        return self::handleOutput($code, $params);
    }

    /**
     * Converti un fichier pdf vers un pdf/a
     *
     * @param string $filename
     * @param string $type
     * @param array  $params
     * @return boolean|string
     */
    protected static function pdfToPdf($filename, $type = 'pdf', array $params = [])
    {
        $params += [
            'output' => self::tmpName($type),
            'unlink' => empty($params['output']),
        ];
        is_file($params['output']) && unlink($params['output']);

        $sh = realpath(dirname(__DIR__).DIRECTORY_SEPARATOR.'Lib'.DIRECTORY_SEPARATOR.'pdf2archive.sh');
        $copy = sprintf(
            'cp %s %s',
            escapeshellarg($filename),
            $tmpname1 = escapeshellarg(self::tmpName('pdf'))
        );
        $tmpname2 = escapeshellarg(self::tmpName('pdf'));
        $cmd = sprintf(
            "%s && %s %s %s && %s && mv %s %s",
            $copy, // certains noms posent problème à Ghostscript
            $sh,
            $tmpname1,
            $tmpname2,
            sprintf('rm %s', $tmpname1),
            $tmpname2,
            escapeshellarg($params['output'])
        );
        if (!empty($params['return_cmd'])) {
            return $cmd;
        }

        static::$logs[] = static::exec($cmd, $strout, $code);

        if ($code === 127) {
            trigger_error("Vous devez installer GhostScript pour effectuer la conversion pdf. Essayez sudo apt install ghostscript");
            return false;
        }

        return self::handleOutput($code, $params);
    }

    /**
     * Permet de lancer une commande en récupérant un array qui contient
     * - La commande envoyé
     * - La sortie standard
     * - La sortie d'erreur
     * - Le code de retour
     *
     * @param string $cmd
     * @param null $output
     * @param null $return_var
     * @return array [string 'cmd', string 'stdout', string 'stderr', int 'code']
     */
    protected static function exec($cmd, &$output = null, &$return_var = null): array
    {
        $stdoutFile = tempnam(sys_get_temp_dir(), 'exec-out-');
        $stderrFile = tempnam(sys_get_temp_dir(), 'exec-err-');
        exec($cmd . ' > '. $stdoutFile .' 2> ' . $stderrFile, $output, $return_var);
        $stdout = file_get_contents($stdoutFile);
        $stderr = file_get_contents($stderrFile);
        unlink($stdoutFile);
        unlink($stderrFile);
        return [
            'cmd' => $cmd,
            'stdout' => $stdout,
            'stderr' => $stderr,
            'code' => $return_var
        ];
    }

    /**
     * Retour de fonction de convertion
     * @param int   $code
     * @param array $params
     * @return string|bool
     */
    protected static function handleOutput(int $code, array $params)
    {
        if (!$params['unlink']) {
            $bin = $params['output'];
        } elseif ($code === 0 && is_file($params['output'])) {
            $bin = file_get_contents($params['output']);
        } else {
            $bin = false;
        }
        is_file($params['output']) && $params['unlink'] && unlink($params['output']);

        return $bin;
    }

    /**
     * Donne un chemin de fichier dans le dossier temporaire avec l'extension cible
     * @param string $type
     * @return string
     */
    protected static function tmpName(string $type): string
    {
        return sys_get_temp_dir().DIRECTORY_SEPARATOR.uniqid('converted-').'.'.$type;
    }
}
